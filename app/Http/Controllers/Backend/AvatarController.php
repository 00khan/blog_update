<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Traits\UploadAvatar;
use Auth;
use Illuminate\Http\Request;
use View;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

class AvatarController extends Controller
{

    use UploadAvatar;

    public function edit()
    {
        $user = Auth::user();

        return View::make('backend.avatar.edit', compact('user'));
    }

    public function update(Request $request)
    {
        $this->getFileContent($request->input('encodedData'));

        $this->getFileExtension();

        if ($this->isValidImage() !== true) {
            return response()->json(["success" => false, "error" => "File is not a valid image"], 422);
        }

        $this->saveAvatarForUser();

        return response()->json(["success" => true]);
    }


    public function update_avatar(Request $request,$id){
        if (\Input::hasFile('avatar')) { 
            $data['avatar'] = time() . '.' . $request->avatar->getClientOriginalExtension();
            $destinationPath    = public_path('images/avatars');   
            $request->avatar->move($destinationPath, $data['avatar']); 

             \DB::table('blog_users')->where('id',$id)->update(['avatar' => $data['avatar'] ]);   
        }
       

        return back();
    }


    


}
