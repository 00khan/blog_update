<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Article\ArticleCreateRequest;
use App\Http\Requests\Article\ArticleUpdateRequest;
use App\Models\Article;
use App\Models\Category;
use App\Models\Tag;
use App\Models\User;
use App\Traits\RelatedArticles;
use Auth;
use View;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;

class ArticleController extends Controller
{
    use RelatedArticles;

    public function index()
    {
        $articles = Article::with('tags', 'author')->orderBy('created_at')->get();

        return View::make('backend.articles.index', compact('articles'));
    }

    public function show($id)
    {
        return redirect()->action('Backend\ArticleController@edit', $id);
    }

    public function edit($id)
    {
        $users = User::all();

        $tags = Tag::all();
  
        $categories = Category::all();

        $article = Article::with('tags', 'author', 'category')->findOrFail($id);

        return View::make('backend.articles.edit', compact('article', 'users', 'categories', 'tags'));
    }

    public function create()
    {
        $users = User::all();

        $tags = Tag::all();

        $categories = Category::all();

        return View::make('backend.articles.edit', compact('users', 'categories', 'tags'));
    }

    public function store(ArticleCreateRequest $request)
    {
        // dd($request->all());
        $article = Article::create($request->getValidRequest());
        // dd($article->id);
        $article->fresh()->updateTags($request->tags);

        if (\Input::hasFile('article_image')) {
            $data['article_image'] = time() . '.' . $request->article_image->getClientOriginalExtension();
            $destinationPath    = public_path('filemanager/userfiles');   
            $request->article_image->move($destinationPath, $data['article_image']);    
        }
        \DB::table('blog_articles')->where('id',$article->id)->update(['article_image' => $data['article_image'] ]);

        return redirect('dashboard/articles')->with('status', 'Article has been created');
    }

    public function update(ArticleUpdateRequest $request, $id)
    {
        // dd($request->all());
        $article = Article::findOrFail($id);

        $result = $article->update($request->getValidRequest());
        // dd($request->tags);
        if(!empty($request->tags)){
            $re = $article->fresh()->updateTags($request->tags);
        }
        
        // dd($re);
        if (\Input::hasFile('article_image')) {
            $data['article_image'] = time() . '.' . $request->article_image->getClientOriginalExtension();
            $destinationPath    = public_path('filemanager/userfiles');   
            $request->article_image->move($destinationPath, $data['article_image']); 
            \DB::table('blog_articles')->where('id',$id)->update(['article_image' => $data['article_image'] ]);   
        }
        // dd("oka");
        
        // dd("oka");

        return redirect()->back()->with('status', 'Article has been updated');
    }

    public function destroy($id)
    {
        Article::findOrFail($id)->delete($id);

        return redirect()->back()->with('status', 'Article has been deleted');
    }

    public function favouriteArticles()
    {
        $articles = Auth::user()->favourites;

        return view('backend.articles.favourites', compact('articles'));
    }

    public function preview($id)
    {
        $article = Article::with('tags', 'author', 'category')->findOrFail($id);

        $relatedArticles = ($this->getRelatedArticles($article)->count() > 3) ? $this->getRelatedArticles($article)->random(3) : $this->getRelatedArticles($article);

        return View::make('frontend.articles.show', compact('article', 'relatedArticles'));

    }
}
