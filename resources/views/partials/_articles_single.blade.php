
<style type="text/css">
  .ui.raised.segments, .ui.raised.segment {
     box-shadow: none !important; 
}
.ui.segments {
    -webkit-box-orient: vertical;
    -webkit-box-direction: normal;
    -ms-flex-direction: column;
    flex-direction: column;
    position: relative;
    margin: 1rem 0em;
     border: none !important; 
     box-shadow: none !important; 
    border-radius: 0.30769231rem;
}
</style>
<div class="backend-dashboard">
    <div class="sidebar hidden-mobile hidden-tablet">
      <div class="ui vertical menu">
        <div class="item text-center">

          <div class="header">
            Blogs Categories
          </div>
          
        </div>
            @include('partials._nav_categories')
           {{--  <a class="item" href="http://127.0.0.1:8000/dashboard/profile">
                
                Profile
            </a>
            <a class="item" href="http://127.0.0.1:8000/dashboard/favourite-articles">
               
                Favourite Articles
            </a>
            <a class="item" href="http://127.0.0.1:8000/dashboard/settings">
                <i class="settings icon"></i>
                Settings
            </a>
            <a class="item" href="http://127.0.0.1:8000/dashboard/tools">
                <i class="wrench layout icon"></i>
                Tools
            </a>
            <a class="item" href="http://127.0.0.1:8000/dashboard/users">
                <i class="users icon"></i>
                Users
            </a>
            <a class="item" href="http://127.0.0.1:8000/dashboard/subscriptions">
                <i class="rss icon"></i>
                Subscriptions
            </a>
            <a class="item" href="http://127.0.0.1:8000/dashboard/articles">
                <i class="file text outline icon"></i>
                Articles
            </a>
            <a class="item" href="http://127.0.0.1:8000/dashboard/categories">
                <i class="list layout icon"></i>
                Categories
            </a>
            <a class="item" href="http://127.0.0.1:8000/dashboard/tags">
                <i class="tags icon"></i>
                Tags
            </a>
            <a class="item" href="http://127.0.0.1:8000/dashboard/media">
                <i class="image icon"></i>
                Media
            </a> --}}
      </div><!--end of menu-->
    </div><!--end of sidebar-->
    <div class="view">
      <div class="ui raised segments">
        <div class="ui segment large">
          <div class="row">
          @foreach($articles as $article)
          <div class="col-md-6">
            <div class="ui card blogger-card fluid teal">
                <div class="content">
                    <div class="right floated meta">{{ $article->published_at->diffForHumans()}}</div>
                    <a href="{{url('about/'.$article->author->slug)}}">
                      <img class="ui avatar mini image" src="{{(!empty($article->author->avatar))? url('images/avatars/'.$article->author->avatar) : url('images/avatars/avatar_default.png')}}">
                    {{$article->author_name}} {{ (!empty($article->author->job))? ', '.$article->author->job : "" }}
                    </a>
                </div>
                <div class="content">
                  <h2><a href="{{url('/blog/'.$article->slug)}}">{{$article->title}}</a></h2>
                  <div class="ui fluid image">
                    <div class="ui teal ribbon label z-index-top">
                      <a href="{{ url('categories/'.$article->category->slug) }}" class="white-font">
                        {{$article->category_name}}
                      </a>
                    </div>
                   {{--  @if(Auth::check())
                      <a class="ui right  teal corner label favorite" href="javascript:void(0)" data-id="{{ $article->id }}" data-content="Add to favourites" data-variation="inverted">
                          <i class="{{($article->isFavourite())? 'yellow active': 'white'}} star icon"></i>
                      </a>
                    @endif --}}

                      <a href="{{url('/blog/'.$article->slug)}}">
                        @if(empty($article->article_image))
                        <img class="ui fluid image lazy hoverable " data-original="{{(!empty($article->article_image))? asset('images'.'/'.$article->article_image): url('images/placeholder_640x480.png')}}"
                                  src="images/placeholder_640x480.png" height="480" width="640" alt="picture">
                        @else
                          <img class="ui fluid image lazy hoverable " data-original="{{(!empty($article->article_image))? asset('images'.'/'.$article->article_image): url('images/placeholder_640x480.png')}}"
                                  src="{{asset('filemanager/userfiles'.'/'.$article->article_image)}}" height="480" width="640" alt="picture">
                        @endif
                       <noscript>
                          <img class="ui fluid image hoverable" height="480" width="640" src="{{(!empty($article->article_image))? asset('images'.'/'.$article->article_image): url('images/placeholder_640x480.png')}}">
                        </noscript> 
                      </a>
                  </div>
                </div>
                <div class="extra content">
                    @include('partials._share_buttons',['article' => $article])
                </div>
            </div>
          </div>
          @endforeach
        </div>
            <div class="ui card blogger-card fluid no-box-shadow text-center">
              {{ $articles->links() }}
            </div>
            <div class="ui card blogger-card fluid " style="display: none;">
              <div class="ui content text-center brand-teal">
                @include('partials._subscribe')
              </div>
            </div>
        </div>
      </div>
    </div>

{{-- @foreach($articles as $article)
<div class="ui card blogger-card fluid teal">
  <div class="content">
    <div class="right floated meta">{{ $article->published_at->diffForHumans()}}</div>
    <a href="{{url('about/'.$article->author->slug)}}"><img class="ui avatar mini image" src="{{(!empty($article->author->avatar))? url('images/avatars/'.$article->author->avatar) : url('images/avatars/avatar_default.png')}}">
        {{$article->author_name}} {{ (!empty($article->author->job))? ', '.$article->author->job : "" }}
    </a>
  </div><!--end of content-->

  <div class="content">
    <h2><a href="{{url('/blog/'.$article->slug)}}">{{$article->title}}</a></h2>
    <div class="ui fluid image">

      <div class="ui teal ribbon label z-index-top">
        <a href="{{ url('categories/'.$article->category->slug) }}" class="white-font">{{$article->category_name}}</a>
      </div><!--end of category-name-->



      @if(Auth::check())
        <a class="ui right  teal corner label favorite" href="javascript:void(0)" data-id="{{ $article->id }}" data-content="Add to favourites" data-variation="inverted">
          <i class="{{($article->isFavourite())? 'yellow active': 'white'}} star icon"></i>
        </a>
      @endif
      <a href="{{url('/blog/'.$article->slug)}}">
        <img class="ui fluid image lazy hoverable " data-original="{{(!empty($article->article_image))? url(config('blogger.filemanager.upload_path').'/'.$article->article_image): url('images/placeholder_640x480.png')}}"
                  src="images/placeholder_640x480.png" height="480" width="640" alt="picture">
       <!--  <noscript>
          <img class="ui fluid image hoverable" height="480" width="640" src="{{(!empty($article->article_image))? url(config('blogger.filemanager.upload_path').'/'.$article->article_image): url('images/placeholder_640x480.png')}}">
        </noscript> -->
      </a>

    </div><!--end of image-->

  </div><!--end of content-->

  <div class="extra content">
      @include('partials._share_buttons',['article' => $article])
  </div><!--end of content-->

</div><!--end of card-->
@endforeach

<div class="ui card blogger-card fluid no-box-shadow text-center">
  {{ $articles->links() }}
</div>

<div class="ui card blogger-card fluid ">
  <div class="ui content text-center brand-teal">
    @include('partials._subscribe')
  </div>
</div> --}}
