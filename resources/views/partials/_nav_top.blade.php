<!--FULL SIZE MENU -->

<section>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
      <div class="row" style="width: 100% !important;">
        <div class="col-sm-12 main_div_menu">
          <a class="navbar-brand js-scroll-trigger" href="#page-top"></a>
          <div style="margin-left: -10px;">
            <div class="navbar-toggler" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
           aria-expanded="false" aria-label="Toggle navigation">
            <button class="navbar-toggler toggle_button" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive"
           aria-expanded="false" aria-label="Toggle navigation" style="position: absolute; margin-top: 15px; margin-left: -3px; cursor: pointer !important;" id="toggle_button" >
           <span class="if-collapsed" id="collapse_menu_open">
            <ul class="menue_bar">
              <li></li>
              <li></li>
              <li></li>
            </ul><span class="toggle_menu">Menu</span>
           </span>
           <span class="if-not-collapsed" id="collapse_menu_close" style="display: none;">
            <ul class="menue_bar">
              <li></li>
            </ul><span class="toggle_menu">Close</span>
           </span>
            </button> </div>
            <div class="mobile_view_menu" style="margin-top: 0px;">
              <ul class="navigation navbar-nav ml-auto" id="nav" style="margin-top: -50px !important; min-height: 111px !important;" >
                <li class="nav-item" id="responsive_logo" style="margin-left: 0px !important;">
                  <a class="active nav-link js-scroll-trigger js-btn" href="https://www.beautyapp.pk" >
                    <img src="{{url('images/logo_responsive.png')}}" class="logo_image_responsive">
                  </a>
                </li>
              </ul>
              <ul class="navigation navbar-nav ml-auto " id="right_side_menu_responsive" style="">
 
              </ul>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <div class="row" style="width: 100%;" id="home_page">
              <div class="col-sm-12">
                <span class="pull-left" id="logo_image">
                  <a class=" nav-link js-scroll-trigger js-btn" href="https://www.beautyapp.pk" >
                    <img class="" src="{{url('images/logo-white.png')}}" style="margin-top: -15px;">
                  </a>
                </span>
                <ul class="navigation navbar-nav ml-auto without_logo_nav with_logo menu_font" id="nav1"  style="margin-right: -40px !important;">

                 {{--  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger js-btn" href="{{url('/')}}">Blog</a>
                  </li> --}}
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger js-btn" href="https://www.beautyapp.pk/products" id="2">Beauty
                      Products</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger js-btn" href="https://www.beautyapp.pk/salon" id="3">Book
                      Salon</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger js-btn" href="https://www.beautyapp.pk/beautytips" id="4">Beauty
                      Tips</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger js-btn" href="https://www.beautyapp.pk/beauty-trend" id="5">Beauty
                      Trends</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger js-btn" href="https://www.beautyapp.pk/fashion-trends" id="6">Fashion
                      Trends</a>
                  </li>
                  
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger js-btn" href="index.php?sec=7#7" id="7" >About
                      Us</a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger js-btn" href="index.php?sec=8#8" id="8" >Contact
                      Us</a>
                  </li>
                   <li class="nav-item active">
                    <a class="nav-link js-scroll-trigger js-btn" href="https://www.beautyapp.pk/blog">Blog</a>
                  </li>
                

                  @if (Auth::guest())
                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger js-btn" href="{{ url('login') }}" >Login/Signup</a>
                    </li>
                    {{-- <li class="nav-item">
                      <a class="nav-link js-scroll-trigger js-btn" href="{{ url('register') }}" >Register</a>
                    </li> --}}
                  @else
                  <li class="nav-item dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="{{ url('dashboard') }}">
                      <img class="ui avatar mini image  hidden-mobile" src="{{(!empty(Auth::user()->avatar))? url('images/avatars/'.Auth::user()->avatar) : url('images/avatars/avatar_default.png') }}">&nbsp;
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">  
                  @if(Session::has( config('blogger.auth.impersonification.session_name')))
                    <li class="nav-item">
                      <a class="nav-link js-scroll-trigger js-btn" href="{{ url('dashboard/back-to-admin-mode') }}" name="back-to-admin-mode">
                        <i class="spy icon"></i> Back to Admin Mode
                      </a>
                    </li>
                  @endif
                   <li class="nav-item">
                      <a class="nav-link js-scroll-trigger js-btn" href="{{ url('/logout') }}" name="logout">
                        <i class="power icon"></i> Log out
                      </a>
                    </li>

                    </ul>
                  </li>
                  @endif

                  

                  <li class="nav-item">
                    <a class="nav-link js-scroll-trigger js-btn" href="salon_admin/adminpage.php" id="9">Admin</a>
                  </li>

                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </nav>
  </section>

<div class="ui fixed inverted menu menu-top">
  <div class="ui container" style="width: 100%;">

    <a href="{{ url('/') }}" class="item">
      <img src="{{ url('images/logo_sm.png') }}" alt="{{config('app.name')}}">
    </a>
    <div class="item hidden-large-desktop hidden-small-desktop">
      <a href="#" class="sidebar-trigger"><i class="sidebar icon"></i></a>
    </div>

    <a class="item icon hidden-mobile hidden-tablet" href="{{ url('/') }}">Blog</a>
    <div class="ui item hidden-mobile hidden-tablet floating  dropdown labeled icon">
      <span class="text">Categories</span>
      <i class="dropdown icon"></i>
      <div class="menu">
          @include('partials._nav_categories')
      </div>
    </div>
    <a class="item hidden-mobile hidden-tablet" href="{{ url('about') }}">About</a>

    @if(config('blogger.search_engine.enabled'))
    <form class="search-form-sm hidden-mobile hidden-tablet" action="{{ url('search') }}">
      <div class="item">
        <div class="ui search">
          <div class="ui icon input">
            <input class="prompt top" type="text" name="query" placeholder="Search articles...">
            <i class="search icon"></i>
          </div>
          <div class="results"></div>
        </div>
      </div>
    </form>
    @endif

    @if (Auth::guest())

      <a class="ui right item" href="{{ url('login') }}">Login</a>
      <a class="ui item" href="{{ url('register') }}">Register</a>

    @else

    <a class="ui right item"  href="{{ url('dashboard') }}"> <img class="ui avatar mini image  hidden-mobile" src="{{(!empty(Auth::user()->avatar))? url('images/avatars/'.Auth::user()->avatar) : url('images/avatars/avatar_default.png') }}">&nbsp;Dashboard</a>
    @if(Session::has( config('blogger.auth.impersonification.session_name')))
       <a class="item hidden-mobile hidden-tablet" href="{{ url('dashboard/back-to-admin-mode') }}" name="back-to-admin-mode"><i class="spy icon"></i> Back to Admin Mode</a>
    @endif
    <a class="item" href="{{ url('/logout') }}" name="logout"><i class="power icon"></i> Log out</a>

    @endif
  </div>
</div>
<!--END OF FULL SIZE MENU-->
