<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if(!empty(Settings::getMetaRobots()))
      <meta name="robots" content="{{ Settings::getMetaRobots() }}">
    @endif
    <!--facebook open graph-->
    <meta name="og:type" content="blog">
    <meta name="og:site_name" content="{{ config('app.name') }}">

    @yield('og-title')
    @yield('og-image')
    @yield('og-description')

    <!--SEO-->
    <meta name="author" content="{{ Settings::getMetaAuthor() }}">
    {{-- <meta name="keywords" content="{{ $meta_keywords or Settings::getMetaKeywords() }}"> --}}
    {{-- <meta name="description" content="{{ $meta_description or Settings::getMetaDescription() }}"> --}}

    <title>{{ config('app.name') }} | @yield('title', Settings::getMetaTitle())</title>

    <!--favicon-->
    <link rel="icon" href="{{ url('images/favicon.ico') }}" type="image/x-icon" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Scripts -->
    <script>
        window.Blogger = {
            url : "{{ config('app.url') }}"
        }
    </script>
    <script src="{{ url('js/app.js') }}" ></script>

    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="{{ url('semantic/semantic.css') }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

  
  <link rel="stylesheet" href="{{asset('ckeditor/samples.css')}}">
  <link rel="stylesheet" href="{{asset('ckeditor/neo.css')}}">
  <link rel="stylesheet" type="text/css" href="{{asset('css/custom.css')}}">


   
</head>

<body>

  @yield('body_content')

  <!-- Scripts -->
  <script src="{{ url('semantic/semantic.js') }}" ></script>

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>



<!-- dependencies (Summernote depends on Bootstrap & jQuery) -->
{{-- <link href="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.css" rel="stylesheet"> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script>
{{-- <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> --}}

<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.3/summernote.js"></script>
<script src="https://vendor/laravel-filemanager/js/stand-alone-button.js"></script>

<!-- markup -->
{{-- <textarea id="summernote-editor" name="content">{!! old('content', $content) !!}</textarea> --}}

<!-- summernote config -->

<script>
  var options = {
    filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
    filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
    filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
    filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
  };
</script>

<script>

var lfm = function(options, cb) {

  var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';

  window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
  window.SetUrl = cb;
}
lfm({type: 'image', prefix: 'prefix'}, function(url, path) {

  $(document).ready(function(){

    // Define function to open filemanager window
    var lfm = function(options, cb) {
      var route_prefix = (options && options.prefix) ? options.prefix : '/laravel-filemanager';
      window.open(route_prefix + '?type=' + options.type || 'file', 'FileManager', 'width=900,height=600');
      window.SetUrl = cb;
    };

    // Define LFM summernote button
    var LFMButton = function(context) {
      var ui = $.summernote.ui;
      var button = ui.button({
        contents: '<i class="note-icon-picture"></i> ',
        tooltip: 'Insert image with filemanager',
        click: function() {

          lfm({type: 'image', prefix: '/laravel-filemanager'}, function(lfmItems, path) {
            lfmItems.forEach(function (lfmItem) {
              context.invoke('insertImage', lfmItem.url);
            });
          });

        }
      });
      return button.render();
    };

    // Initialize summernote with LFM button in the popover button group
    // Please note that you can add this button to any other button group you'd like
    $('#summernote-editor').summernote({
      toolbar: [
        ['popovers', ['lfm']],
      ],
      buttons: {
        lfm: LFMButton
      }
    })
  });
</script>
<script language="javascript">

        $(function(){ 
         var navMain = $(".navbar-collapse");
         navMain.on("click", "a", null, function () {
             navMain.collapse('hide');
             document.getElementById("responsive_logo").style.display = "block";
            $( "#collapse_menu_open" ).toggle();
            $( "#collapse_menu_close" ).toggle();
            // $( "#right_side_menu_responsive" ).toggle();
         });
     });

    $( "#toggle_button" ).click(function() {
            document.getElementById("responsive_logo").style.display = "block";
            $( "#collapse_menu_open" ).toggle();
            $( "#collapse_menu_close" ).toggle();
            // $( "#right_side_menu_responsive" ).toggle();
        });


        $(function(){ 
         var navMain = $(".navbar-collapse");
         navMain.on("click", "a", null, function () {
             navMain.collapse('hide');
             document.getElementById("responsive_logo").style.display = "block";
            $( "#collapse_menu_open" ).toggle();
            $( "#collapse_menu_close" ).toggle();
            // $( "#right_side_menu_responsive" ).toggle();
         });
     });


        $("#contact_us").click(function() {
            $('html,body').animate({
                scrollTop: $(".contact_us").offset().top-200},
                'slow');
        });


        $('#toggle_button').click(function(e){
                e.stopPropagation();
                 $('#navbarResponsive').toggleClass('show');
            });
            $('#navbarResponsive').click(function(e){
                e.stopPropagation();
            });
        $('body,html').click(function(e){
            document.getElementById("collapse_menu_open").style.display = "block";
            document.getElementById("collapse_menu_close").style.display = "none";
            $('#navbarResponsive').removeClass('show');

        });

    </script>   


  @yield('body_scripts')

  <script>
      initSample();
  </script>

  @include('partials._google_analytics')

</body>
</html>
