

@extends('layouts.backend')

@section('title', 'Avatar')

@section('content')
<style type="text/css">
  .btn_upload {
  cursor: pointer;
  display: inline-block;
  overflow: hidden;
  position: relative;
  color: #fff;
  background-color: #2a72d4;
  border: 1px solid #166b8a;
  padding: 5px 10px;
}

.btn_upload:hover,
.btn_upload:focus {
  background-color: #7ca9e6;
}

.yes {
  display: flex;
  align-items: flex-start;
  margin-top: 10px !important;
}

.btn_upload input {
  cursor: pointer;
  height: 100%;
  position: absolute;
  filter: alpha(opacity=1);
  -moz-opacity: 0;
  opacity: 0;
}

.it {
  height: 100px;
  margin-left: 10px;
}

.btn-rmv1,
.btn-rmv2,
.btn-rmv3,
.btn-rmv4,
.btn-rmv5 {
  display: none;
}

.rmv {
  cursor: pointer;
  color: #fff;
  border-radius: 30px;
  border: 1px solid #fff;
  display: inline-block;
  background: rgba(255, 0, 0, 1);
  margin: -5px -10px;
}

.rmv:hover {
  background: rgba(255, 0, 0, 0.5);
}

</style>
<div class="ui segment large">
  {!! Breadcrumbs::render('backend.avatar') !!}
</div><!--end of segment-->

<div class="ui segment teal padded">
<h2>Upload Avatar</h2>

<form action="{{url('upload_avatar'.'/'.Auth::user()->id)}}" method="post" enctype="multipart/form-data">
  {{csrf_field()}}
  <div class="yes">
    <span class="btn_upload">
      <input type="file" id="imag" name="avatar" title="" class="input-img"/>
      Choose Image
      </span>
    <img id="ImgPreview" src="" class="preview1" />
    {{-- <input type="button" id="removeImage1" value="x" class="btn-rmv1" /> --}}
  </div>
  <br><br>
  <div class="col-12">
    <input type="submit" value="Update Avatar" class="btn btn-primary">
  </div>
 </form>
</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript" src="{{ url('js/croppie.min.js') }}"></script>
<script>
$(document).ready(function() {
  var croppedFile;
     $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 200,
            height: 200,
            type: 'square'
        },
        boundary: {
            width: 300,
            height: 300
        }
    });
    function isNotAnImage(file){
        var fileType = file["type"];
    var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
    if ($.inArray(fileType, ValidImageTypes) < 0) {
      alert("File must be an image");
      resetUpload();
      return false;
    }
    }
    function readFile(input) {
        if (input.files && input.files[0]) {
          var file = input.files[0];
          if(isNotAnImage(file)){
            return false;
          }
            var reader = new FileReader();
            reader.onload = function (e) {
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                });
            }
            reader.readAsDataURL(file);
        }
    }
    function resetUpload(){
      $uploadCrop.croppie('bind', {
         url: "{{ (!empty(Auth::user()->avatar))? url('images/avatars/'.Auth::user()->avatar) : url('images/avatars/avatar_default.png') }}"
      });
      document.querySelector("#avatar-form").reset();
    }
    $('#upload-avatar').click(function(){
    $('#avatar-modal') .modal('show');
    resetUpload();
  });
  $('#avatar-modal').modal({
    onHidden : function(){
      resetUpload();
    }
  });
    $('#upload').on('change', function () { readFile(this); });
    $('#avatar-update').click(function () {
        $uploadCrop.croppie('result', {
            type: 'canvas',
            size:{ width: 200, height: 200 }
        }).then(function (croppedImage) {
          $.ajax({
        type: "POST",
        url: "{{ url('dashboard/avatar') }}",
        data: { encodedData : croppedImage },
        cache: false,
      }).done(function(response){
        location.reload();
      }).fail(function(error){
        console.log(error);
      });
        });
    });
});



function readURL(input, imgControlName) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    reader.onload = function(e) {
      $(imgControlName).attr('src', e.target.result);
    }
    reader.readAsDataURL(input.files[0]);
  }
}

$("#imag").change(function() {
  // add your logic to decide which image control you'll use
  var imgControlName = "#ImgPreview";
  readURL(this, imgControlName);
  $('.preview1').addClass('it');
  $('.btn-rmv1').addClass('rmv');
});
</script>

@endsection
